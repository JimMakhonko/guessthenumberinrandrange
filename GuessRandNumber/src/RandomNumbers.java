import java.util.*;

public class RandomNumbers extends RandomNumber {
    @Override
    public int rand(int min, int max) {
        return super.rand(min, max);
    }

    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        System.out.println("Entering the bounds...");
        System.out.print("Enter Min value: ");
        int min = scanner.nextInt();

        System.out.print("Enter Max value: ");
        int max = scanner.nextInt();

        RandomNumber randomNumber = new RandomNumber();
        int secretNumber = randomNumber.rand(min, max);

        int guess;
        List<Integer> inputs = new ArrayList<>();
        do {
            System.out.print("Enter a guess (" + min + " - " + max + "): ");
            guess = scanner.nextInt();
            inputs.add(guess);
            if (guess == secretNumber) {
                System.out.println("Correct");
                System.out.print("Your inputs: " + Arrays.toString(inputs.stream().filter(Objects::nonNull).toArray()));
            } else if (guess < secretNumber) {
                System.out.println("Bigger");
            } else {
                System.out.println("Smaller");
            }
        } while (guess != secretNumber);
    }
}
